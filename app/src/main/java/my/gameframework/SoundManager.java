package my.gameframework;

import java.util.HashMap;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundManager {

	private volatile static SoundManager instance;

	private SoundPool soundPool;
	private HashMap<Integer, Integer> soundPoolMap;
	private AudioManager audioManager;
	private Context context;

	public static SoundManager getInstance() {
		if (instance == null) {
			synchronized (SoundManager.class) {
				if (instance == null) {
					instance = new SoundManager();
				}
			}
		}

		return instance;
	}

	public void init(Context context) {
		this.context = context;
		this.soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);
		this.soundPoolMap = new HashMap<Integer, Integer>();
		this.audioManager = (AudioManager) context
				.getSystemService(Context.AUDIO_SERVICE);
	}

	private SoundManager() {
	}

	public void addSound(int resourceId) {
		int soundId = soundPool.load(context, resourceId, 1);
		soundPoolMap.put(resourceId, soundId);
	}

	public void play(int resourceid) {
		float streamVolume = audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC)
				/ audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		soundPool.play((Integer) soundPoolMap.get(resourceid), streamVolume,
				streamVolume, 1, 0, 1.f);
	}

	public void playLooped(int resourceId) {
		float streamVolume = audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC)
				/ audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		soundPool.play((Integer) soundPoolMap.get(resourceId), streamVolume,
				streamVolume, 1, -1, 1.f);
	}

}

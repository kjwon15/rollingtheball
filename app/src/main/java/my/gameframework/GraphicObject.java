package my.gameframework;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class GraphicObject {

	protected Bitmap bitmap;
	protected int x;
	protected int y;
    protected Rect boundBox;

	public GraphicObject(Bitmap bitmap) {
		this.bitmap = bitmap;
		x = 0;
		y = 0;

        boundBox = new Rect(x, y, x + bitmap.getWidth(), y + bitmap.getHeight());
	}

	public void draw(Canvas canvas) {
		canvas.drawBitmap(bitmap, x, y, null);
	}
	
	public void setPosition(int x, int y) {
		this.x = x;
		this.y = y;

        boundBox.set(x, y, x + bitmap.getWidth(), y + bitmap.getHeight());
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

    public Rect getBoundBox() {
        return boundBox;
    }

}

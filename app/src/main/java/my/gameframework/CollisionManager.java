package my.gameframework;

import android.graphics.Rect;

public class CollisionManager {
	public static boolean checkBoxToBox(Rect r1, Rect r2) {
		return Rect.intersects(r1, r2);
	}

    public static boolean collisionEnough(Rect r1, Rect r2) {
        return r1.contains(r2.centerX(), r2.centerY());
    }
}

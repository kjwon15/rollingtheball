package my.gameframework;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Sprite {
	private Bitmap animation;
	private int xPos;
	private int yPos;
	private Rect rect;
	private int fps;
	private int frames;
	private int currentFrame;
	private long frameTimer;
	private int spriteHeight;
	private int spriteWidth;

	public Sprite() {
		rect = new Rect(0, 0, 0, 0);
		frameTimer = 0;
		currentFrame = 0;
		xPos = 80;
		yPos = 200;
	}

	public void initialize(Bitmap bitmap, int height, int width, int fps,
			int frameCount) {
		animation = bitmap;
		spriteHeight = height;
		spriteWidth = width;
		rect.top = 0;
		rect.bottom = spriteHeight;
		rect.left = 0;
		rect.right = spriteWidth;
		this.fps = 1000 / fps;
		frames = frameCount;
	}

	public void update(long gameTime) {
		if (gameTime > frameTimer + fps) {
			frameTimer = gameTime;
			currentFrame += 1;

			if (currentFrame >= frames) {
				currentFrame = 0;
			}
		}

		rect.left = currentFrame * spriteWidth;
		rect.right = rect.left + spriteWidth;
	}

	public void draw(Canvas canvas) {
		Rect rect = new Rect(xPos, yPos, xPos + spriteWidth, yPos
				+ spriteHeight);

		canvas.drawBitmap(animation, rect, rect, null);
	}
}

package my.gameframework;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class AppManager {

    private volatile static AppManager instance;

    private GameView gameView;
    private Resources resources;

    public int screenWidth;
    public int screenHeight;

    private long score;

    public static AppManager getInstance() {
        if (instance == null) {
            synchronized (AppManager.class) {
                if (instance == null) {
                    instance = new AppManager();
                }
            }
        }

        return instance;
    }

    private AppManager() {
    }

    public Bitmap getBitmap(int id) {
        return BitmapFactory.decodeResource(resources, id);
    }

    public void setGameView(GameView gameView) {
        this.gameView = gameView;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public GameView getGameView() {
        return gameView;
    }

    public Resources getResources() {
        return resources;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }
}

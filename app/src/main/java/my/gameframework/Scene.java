package my.gameframework;

import android.content.Context;
import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.MotionEvent;

public abstract class Scene {
    public enum State {
        NORMAL,
        END,
    };

    public State state = State.NORMAL;

	public abstract void init(Context context);

	public abstract void destroy();

	public abstract void update();

	public abstract void render(Canvas canvas);

	public abstract boolean onKeyDown(int keyCode, KeyEvent event);

	public abstract boolean onTouchEvent(MotionEvent event);
}

package my.gameframework;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

public class SpriteAnimation extends GraphicObject {

	private Rect rect;
	private int fps;
	private int frames;
	private int currentFrame;

	private long frameTimer;

	private int height;
	private int width;

	protected boolean reply = true;
	protected boolean end = false;

	public SpriteAnimation(Bitmap bitmap) {
		super(bitmap);
		rect = new Rect(0, 0, 0, 0);
		frameTimer = 0;
		currentFrame = 0;
	}

	public void initSpriteData(int Height, int Width, int theFPS,
			int theFrameCount) {
		height = Height;
		width = Width;
		rect.top = 0;
		rect.bottom = height;
		rect.left = 0;
		rect.right = width;
		fps = 1000 / theFPS;
		frames = theFrameCount;
	}

	@Override
	public void draw(Canvas canvas) {
		Rect dest = new Rect(x, y, x + width, y + height);

		canvas.drawBitmap(bitmap, rect, dest, null);
	}

	public void update(long GameTime) {
		if (!end) {
			if (GameTime > frameTimer + fps) {
				frameTimer = GameTime;
				currentFrame += 1;

				if (currentFrame >= frames) {
					if (reply)
						currentFrame = 0;
					else
						end = true;
				}
			}
		}

		rect.left = currentFrame * width;
		rect.right = rect.left + width;
	}

	public boolean getAnimationEnd() {
		return end;
	}

}

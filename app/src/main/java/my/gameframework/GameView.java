package my.gameframework;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import kai.game.rollingtheball.AccSensorManager;
import kai.game.rollingtheball.GameScene;
import kai.game.rollingtheball.IntroScene;
import kai.game.rollingtheball.ResultScene;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

	private GameViewThread thread;
	private AppManager appManager;
	private Scene scene;
    private Context context;

	public GameView(Context context) {
		super(context);
        this.context = context;
		getHolder().addCallback(this);
		thread = new GameViewThread(getHolder(), this);

		appManager = AppManager.getInstance();
		appManager.setGameView(this);
		appManager.setResources(getResources());

        AccSensorManager.getInstance().init(context);

        IntroScene introState = new IntroScene();
		changeGameScene(introState);

	}

	public void update() {
		scene.update();

        if (scene.state == Scene.State.END) {
            if (scene instanceof IntroScene) {
                changeGameScene(new GameScene());
            } else if (scene instanceof GameScene) {
                changeGameScene(new ResultScene());
            } else if (scene instanceof ResultScene) {
                changeGameScene(new IntroScene());
            }
        }
	}
	
	public void changeGameScene(Scene newScene) {
		if (scene != null) {
			scene.destroy();
		}
		
		newScene.init(context);
		scene = newScene;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);

		scene.render(canvas);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		scene.onKeyDown(keyCode, event);
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		scene.onTouchEvent(event);
		return super.onTouchEvent(event);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        appManager.screenWidth = width;
        appManager.screenHeight = height;
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		boolean retry = true;
		thread.setRunning(false);
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {

			}
		}
	}

}

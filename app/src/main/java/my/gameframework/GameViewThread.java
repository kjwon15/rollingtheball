package my.gameframework;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class GameViewThread extends Thread {

	private SurfaceHolder surfaceHolder;
	private GameView gameView;
	private boolean running = false;

	public GameViewThread(SurfaceHolder surfaceHolder, GameView gameView) {
		this.surfaceHolder = surfaceHolder;
		this.gameView = gameView;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	@Override
	public void run() {
		Canvas canvas;
		while (running) {
			canvas = null;
			try {
                canvas = surfaceHolder.lockCanvas(null);
                gameView.update();
                synchronized (surfaceHolder) {
                    gameView.onDraw(canvas);
                }
            } catch (NullPointerException e) {
            } finally {
				if (canvas != null) {
					surfaceHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}
}

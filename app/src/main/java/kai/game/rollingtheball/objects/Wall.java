package kai.game.rollingtheball.objects;

import android.graphics.Canvas;

import kai.game.rollingtheball.R;
import my.gameframework.AppManager;
import my.gameframework.GraphicObject;

/**
 * Created by kjwon15 on 14. 11. 29..
 */
public class Wall extends GraphicObject {

    public Wall(int x, int y) {
        super(AppManager.getInstance().getBitmap(R.drawable.wall));
        this.setPosition(x, y);
    }
}

package kai.game.rollingtheball.objects;

import android.graphics.Bitmap;

import kai.game.rollingtheball.R;
import my.gameframework.AppManager;
import my.gameframework.GraphicObject;

/**
 * Created by kjwon15 on 14. 11. 29..
 */
public class Ball extends GraphicObject{
    private int x;
    private int y;
    public Ball(int x, int y) {
        super(AppManager.getInstance().getBitmap(R.drawable.ball));
        this.setPosition(x, y);
    }
}

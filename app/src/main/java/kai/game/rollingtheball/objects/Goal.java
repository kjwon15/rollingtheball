package kai.game.rollingtheball.objects;

import android.graphics.Bitmap;

import kai.game.rollingtheball.R;
import my.gameframework.AppManager;
import my.gameframework.GraphicObject;

/**
 * Created by kjwon15 on 14. 12. 4..
 */
public class Goal extends GraphicObject {
    public Goal(int x, int y) {
        super(AppManager.getInstance().getBitmap(R.drawable.target));
        this.setPosition(x, y);
    }
}

package kai.game.rollingtheball;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * Created by kjwon15 on 14. 12. 9..
 */
public class AccSensorManager {
    private volatile static AccSensorManager instance = null;

    private double accX;
    private double accY;

    private SensorManager sensorManager;
    private SensorEventListener accListener;

    private AccSensorManager() {
    }

    public void init(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        if (accListener == null) {
            Sensor accSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            accListener = new AccelerometerListener();
            sensorManager.registerListener(accListener, accSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    public static AccSensorManager getInstance() {
        if (instance == null) {
            synchronized (AccSensorManager.class) {
                if (instance == null) {
                    instance = new AccSensorManager();
                }
            }
        }
        return instance;
    }

    private class AccelerometerListener implements SensorEventListener {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            accX = -sensorEvent.values[0];
            accY = sensorEvent.values[1];
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    }

    public double getAccX() {
        return accX;
    }

    public double getAccY() {
        return accY;
    }
}

package kai.game.rollingtheball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.KeyEvent;
import android.view.MotionEvent;

import my.gameframework.AppManager;
import my.gameframework.Scene;

/**
 * Created by kjwon15 on 14. 12. 13..
 */
public class ResultScene extends Scene {

    AppManager appManager;
    long record;
    float[] hsv = new float[]{0.f, 1.0f, 0.4f};

    @Override
    public void init(Context context) {
        appManager = AppManager.getInstance();
        record = appManager.getScore();
    }

    @Override
    public void destroy() {

    }

    @Override
    public void update() {
        hsv[0] = hsv[0] + .3f % 255;
    }

    @Override
    public void render(Canvas canvas) {
        int screenCenterX = appManager.screenWidth / 2;
        int screenCenterY = appManager.screenHeight / 2;

        canvas.drawColor(Color.HSVToColor(hsv));

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);

        paint.setTextSize(50);
        canvas.drawText("Result: " + record, screenCenterX, screenCenterY, paint);

        paint.setTextSize(30);
        canvas.drawText("Touch anywhere to start new game.", screenCenterX, screenCenterY + 60, paint);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.state = State.END;
        return false;
    }
}

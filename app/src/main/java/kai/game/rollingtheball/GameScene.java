package kai.game.rollingtheball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;

import java.util.ArrayList;

import kai.game.rollingtheball.objects.Ball;
import kai.game.rollingtheball.objects.Goal;
import kai.game.rollingtheball.objects.Wall;
import my.gameframework.AppManager;
import my.gameframework.CollisionManager;
import my.gameframework.Scene;

/**
 * Created by kjwon15 on 14. 11. 29..
 */
public class GameScene extends Scene {
    ArrayList<Wall> walls = new ArrayList<Wall>();
    Ball ball;
    Goal goal;

    AccSensorManager accSensorManager;

    double ballSpeedX = 0;
    double ballSpeedY = 0;

    long startTime;

    @Override
    public void init(Context context) {
        accSensorManager = AccSensorManager.getInstance();

        startTime = System.currentTimeMillis();

        createMap();
    }

    private void createMap() {
        final int blockGap = 70;

        int[][] positions = {
                {0, 0}, {1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0}, {0, 1}, {2, 1}, {8, 1}, {9, 1}, {0, 2}, {2, 2}, {5, 2}, {7, 2}, {9, 2}, {0, 3}, {2, 3}, {4, 3}, {7, 3}, {9, 3}, {0, 4}, {2, 4}, {4, 4}, {6, 4}, {9, 4}, {0, 5}, {2, 5}, {4, 5}, {6, 5}, {9, 5}, {0, 6}, {2, 6}, {4, 6}, {6, 6}, {8, 6}, {9, 6}, {0, 7}, {2, 7}, {4, 7}, {6, 7}, {9, 7}, {0, 8}, {4, 8}, {7, 8}, {9, 8}, {0, 9}, {2, 9}, {4, 9}, {6, 9}, {9, 9}, {0, 10}, {2, 10}, {4, 10}, {7, 10}, {9, 10}, {0, 11}, {2, 11}, {5, 11}, {7, 11}, {9, 11}, {0, 12}, {2, 12}, {3, 12}, {5, 12}, {9, 12}, {0, 13}, {5, 13}, {7, 13}, {9, 13}, {0, 14}, {1, 14}, {2, 14}, {3, 14}, {4, 14}, {5, 14}, {6, 14}, {7, 14}, {8, 14}, {9, 14}
        };

        for (int[] position : positions) {
            walls.add(new Wall(position[0] * blockGap, position[1] * blockGap));
        }

        ball = new Ball(1 * blockGap, 1 * blockGap);
        goal = new Goal(8 * blockGap, 2 * blockGap);
    }

    @Override
    public void destroy() {
    }

    @Override
    public void update() {
        int x = ball.getX();
        int y = ball.getY();

        ballSpeedX += accSensorManager.getAccX() / 5;
        ballSpeedY += accSensorManager.getAccY() / 5;

        limitSpeed();
        collisionBall();

        x += ballSpeedX;
        y += ballSpeedY;
        ball.setPosition(x, y);



        if (CollisionManager.collisionEnough(goal.getBoundBox(), ball.getBoundBox())) {
            long elapsedTime = (System.currentTimeMillis() - startTime) / 1000;
            AppManager.getInstance().setScore(elapsedTime);
            this.state = State.END;
        }
    }

    private void collisionBall() {
        final double friction = 0.7;

        Rect box = ball.getBoundBox();
        int ballCenterX = box.left + box.width() / 2;
        int ballCenterY = box.top + box.height() / 2;

        for (Wall wall : walls) {
            Rect wallBox = wall.getBoundBox();
            if (Rect.intersects(box, wallBox)) {
                if (box.left < wallBox.right && wallBox.right < ballCenterX) {
                    ballSpeedX = Math.abs(ballSpeedX) * friction;
                } else if (box.right > wallBox.left && wallBox.left > ballCenterX) {
                    ballSpeedX = -Math.abs(ballSpeedX) * friction;
                }

                if (box.top < wallBox.bottom && wallBox.bottom < ballCenterY) {
                    ballSpeedY = Math.abs(ballSpeedY) * friction;
                } else if (box.bottom > wallBox.top && wallBox.top > ballCenterY) {
                    ballSpeedY = -Math.abs(ballSpeedY) * friction;
                }
            }
        }
    }

    private void limitSpeed() {
        final int limit = 20;

        if (ballSpeedX < -limit) {
            ballSpeedX = -limit;
        } else if (ballSpeedX > limit) {
            ballSpeedX = limit;
        }

        if (ballSpeedY < -limit) {
            ballSpeedY = -limit;
        } else if (ballSpeedY > limit) {
            ballSpeedY = limit;
        }
    }

    @Override
    public void render(Canvas canvas) {
        for (Wall wall : walls) {
            wall.draw(canvas);
        }
        goal.draw(canvas);
        ball.draw(canvas);

        long elapsedTime = (System.currentTimeMillis() - startTime) / 1000;
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(40);

        canvas.drawText("elapsed time: " + elapsedTime, 0, 50, paint);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}

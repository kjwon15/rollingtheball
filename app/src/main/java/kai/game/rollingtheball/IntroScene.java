package kai.game.rollingtheball;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;

import kai.game.rollingtheball.objects.Ball;
import my.gameframework.AppManager;
import my.gameframework.Scene;

/**
 * Created by kjwon15 on 14. 12. 4..
 */
public class IntroScene extends Scene {
    AccSensorManager accSensorManager = AccSensorManager.getInstance();
    AppManager appManager = AppManager.getInstance();

    Ball ball;
    double ballSpeedX;
    double ballSpeedY;

    @Override
    public void init(Context context) {
        ballSpeedX = 0;
        ballSpeedY = 0;
        ball = new Ball(0, 0);
    }

    @Override
    public void destroy() {

    }

    @Override
    public void update() {
        int x = ball.getX();
        int y = ball.getY();

        ballSpeedX += accSensorManager.getAccX() / 5;
        ballSpeedY += accSensorManager.getAccY() / 5;

        limitSpeed();
        collisionBall();

        x += ballSpeedX;
        y += ballSpeedY;
        ball.setPosition(x, y);

    }

    private void limitSpeed() {
        final int limit = 20;

        if (ballSpeedX < -limit) {
            ballSpeedX = -limit;
        } else if (ballSpeedX > limit) {
            ballSpeedX = limit;
        }

        if (ballSpeedY < -limit) {
            ballSpeedY = -limit;
        } else if (ballSpeedY > limit) {
            ballSpeedY = limit;
        }
    }

    private void collisionBall() {
        final double friction = 0.7;

        Rect box = ball.getBoundBox();

        if (box.left < 0) {
            ballSpeedX = Math.abs(ballSpeedX) * friction;
        } else if (box.right > appManager.screenWidth) {
            ballSpeedX = -Math.abs(ballSpeedX) * friction;
        }

        if (box.top < 0) {
            ballSpeedY = Math.abs(ballSpeedY) * friction;
        } else if (box.bottom > appManager.screenHeight) {
            ballSpeedY = -Math.abs(ballSpeedY) * friction;
        }

    }

    @Override
    public void render(Canvas canvas) {
        ball.draw(canvas);

        Paint paint = new Paint();
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setColor(Color.WHITE);
        paint.setTextSize(30);

        canvas.drawText("Touch anywhere to start a game",
                appManager.screenWidth / 2, appManager.screenHeight / 2, paint);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.state = State.END;
        return false;
    }
}
